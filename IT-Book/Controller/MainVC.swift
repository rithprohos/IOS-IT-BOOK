//
//  MainVC.swift
//  IT-Book
//
//  Created by Som Rith Prohos on 10/16/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner
import MBProgressHUD
import Moya

class MainVC: UIViewController , UITableViewDelegate , UITableViewDataSource , UISearchBarDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var books : [Book] = []
    var filterBooks : [Book] = []
    var paging : Int = 1
    var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showLoadingIndicator()
        let provider = MoyaProvider<MyAPI>()
        provider.request(.search(bookName: "java")) { result in
            switch result {
            case let .success(moyaResponse):
                let data = moyaResponse.data
                let json = JSON(data: data) // convert network data to json
                print("---------- \(json["Books"][0]["Image"])")
                if let resData = json["Books"].arrayObject {
                    let rest = resData as! [[String : AnyObject]]
                    //                    self.arrRes = resData as! [[String:AnyObject]]
                    self.parseJSONToArrayModel(restData: rest)
                    print(self.books.count)
                }
                if self.books.count > 0 {
                    self.hideLoadingIndicator()
                    self.tableView.reloadData()
                }
            case let .failure(error):
                print("error: \(error)")
            }
        }
        
//        showLoadingIndicator()
//        Alamofire.request("http://it-ebooks-api.info/v1/search/php%20mysql").responseJSON { (responseData) -> Void in
//            if((responseData.result.value) != nil) {
//                let swiftyJsonVar = JSON(responseData.result.value!)
//                if let resData = swiftyJsonVar["Books"].arrayObject {
//                    let rest = resData as! [[String : AnyObject]]
////                    self.arrRes = resData as! [[String:AnyObject]]
//                    self.parseJSONToArrayModel(restData: rest)
//                    print(self.books.count)
//                }
//                if self.books.count > 0 {
//                    self.hideLoadingIndicator()
//                    self.tableView.reloadData()
//                }
//
//            }
//        }
        
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.search
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BookCell", for: indexPath) as? BookCell {
            
//            let dict : [String : AnyObject]
//            if isSearching {
//                dict = filterData[indexPath.row]
//            } else {t
//                dict = arrRes[indexPath.row]
//            }
//
//            let image = dict["Image"] as? String
//            let title = dict["Title"] as? String
//            let description = dict["Description"] as? String
//            cell.renderCell(book: Book(imageUrl: image!, title: title!, description: description!))
            
            let book : Book
            if isSearching {
                book = filterBooks[indexPath.row]
            } else {
                book = books[indexPath.row]
            }
            print(book.title)
            
            cell.renderCell(book: book)
            
            return cell
        }else {
            return UITableViewCell()
        }
    }
    
    // row of table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filterBooks.count
        } else {
            return books.count
        }
    }
    
    // set high for table view
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == books.count - 1 {
            // request more item
            paging+=1
            requestMoreData(paging: paging)
            
            
            
        }
    }
    
    // select item of table view
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book : AnyObject
        if isSearching {
            book = books[indexPath.row]
        } else {
            book = books[indexPath.row]
        }
//        let image = dict["Image"] as? String
//        let title = dict["Title"] as? String
//        let description = dict["Description"] as? String
        
//        let book = Book(imageUrl: image!, title: title!, description: description!)
//        print(book.title)
        
        performSegue(withIdentifier: "BookVC", sender: book)
    }
    
    // prepare segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? BookVC {
            if let book = sender as? Book {
                destination.book = book
            }
        }
    }
    
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if searchText == "" {
//            isSearching = false
//            view.endEditing(true)
//            tableView.reloadData()
//        }else {
//            isSearching = true
//            requestFilterData(searchingText: searchText)
//        }
//    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        if searchBar.text == "" {
            isSearching = false
            tableView.reloadData()
        }else {
            isSearching = true
            requestFilterData(searchingText: searchBar.text!)
        }
    }
    
    
    func requestMoreData(paging : Int) {
        Alamofire.request("http://it-ebooks-api.info/v1/search/php%20mysql/page/\(paging)").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                if let resData = swiftyJsonVar["Books"].arrayObject {
                    let data = resData as! [[String:AnyObject]]
//                    self.arrRes.append(contentsOf: data)
                    self.parseJSONToArrayModel(restData: data)
                }
                if self.books.count > 0 {
                    self.tableView.reloadData()
                }
                
            }
        }
    }
    
    func requestFilterData(searchingText : String) {
        showLoadingIndicator()
        Alamofire.request("http://it-ebooks-api.info/v1/search/\(searchingText)").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                if let resData = swiftyJsonVar["Books"].arrayObject {
//                    self.filterData = resData as! [[String:AnyObject]]
                    self.parseJSONToArrayModel(restData: resData as! [[String : AnyObject]])
                }
                if self.filterBooks.count > 0 {
                    self.hideLoadingIndicator()
                    self.tableView.reloadData()
                }
                
            }
        }
    }
    
    func showLoadingIndicator() {
        let spiningActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spiningActivity.labelText = "loading"
    }
    
    func hideLoadingIndicator() {
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
    
    func parseJSONToArrayModel(restData : [[String : AnyObject]]) {
        for index in 0 ..< restData.count {
            let dict = restData[index]
            let image = dict["Image"] as? String
            let title = dict["Title"] as? String
            let description = dict["Description"] as? String
            if isSearching {
                self.filterBooks.append(Book(imageUrl: image!, title: title!, description: description!))
            }else {
                self.books.append(Book(imageUrl: image!, title: title!, description: description!))
            }
            //                        print(self.books[index].title)
        }
    }
    
    


}

