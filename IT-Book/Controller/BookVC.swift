//
//  BookVC.swift
//  IT-Book
//
//  Created by Som Rith Prohos on 10/16/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import UIKit

class BookVC: UIViewController {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bookLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    private var _book : Book!
    
    var book : Book {
        get {
            return _book
        }
        set {
            _book = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bookLbl.text = book.title
        descriptionLbl.text = book.description
        
        let url = URL(string: book.imageUrl)!
        
//        DispatchQueue.global().async {
//            do {
//                let data = try Data(contentsOf: url)
//                DispatchQueue.main.sync {
//                    self.imageView.image = UIImage(data: data)
//                }
//            } catch {
//
//            }
//        }
        
        imageView.sd_setImage(with: url, placeholderImage: nil, options: [], completed: nil)

    }
    
   
    

  

}
