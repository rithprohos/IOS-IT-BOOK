//
//  Book.swift
//  IT-Book
//
//  Created by Som Rith Prohos on 10/16/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import Foundation
import SwiftyJSON

class Book  {
    private var _imageUrl : String!
    private var _title : String!
    private var _description : String!
    
    var imageUrl : String {
        return _imageUrl
    }
    
    var title : String {
        return _title
    }
    
    var description : String {
        return _description
    }
    
    init(imageUrl : String , title : String , description : String) {
        _imageUrl = imageUrl
        _title = title
        _description = description
    }
    
}
