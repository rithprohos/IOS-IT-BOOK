//
//  ServerAPI.swift
//  IT-Book
//
//  Created by Som Rith Prohos on 10/18/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import Foundation
import Moya

enum MyAPI {
    case all
    case search(bookName : String)
}

extension MyAPI : TargetType {
    var baseURL: URL {
        return URL(string: "http://it-ebooks-api.info/v1/search")!
    }
    
    var path: String {
        switch self {
        case .all:
            return "/php%20mysql"
        case .search(bookName: let bookName) :
            return "/\(bookName)"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
    
    
    
}


