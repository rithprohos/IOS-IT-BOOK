//
//  BookCellTableViewCell.swift
//  IT-Book
//
//  Created by Som Rith Prohos on 10/16/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import UIKit
import SDWebImage

class BookCell : UITableViewCell {

   
    @IBOutlet weak var bookImageView: UIImageView!
    @IBOutlet weak var bookTitle: UILabel!
    @IBOutlet weak var bookDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func renderCell(book : Book) {
        bookTitle.text = book.title
        bookDescription.text = book.description
        
        let url = URL(string: book.imageUrl)!
        
        bookImageView.sd_setImage(with: url, placeholderImage: nil, options: [], completed: nil)
        
//        DispatchQueue.global().async {
//            do {
//                let data = try Data(contentsOf: url)
//                DispatchQueue.main.sync {
//                    self.bookImageView.image = UIImage(data: data)
//                }
//            } catch {
//
//            }
//        }
        
        
    }

    
   
    
}
